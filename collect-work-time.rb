#
# Copyright (C) 2019  Horimoto Yasuhiro <horimoto@clear-code.com>
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require "redmine_rest"

class Client
  BASE_URL = ENV["REDMINE_BASE_URL"]
  KEY = ENV["REDMINE_API_ACCESS_KEY"]

  def collect_work_time(prefix, user_ids)
    RedmineRest::Models.configure_models apikey: KEY,
                                         site: BASE_URL,
                                         prefix: prefix
    include RedmineRest::Models
    TimeEntry.find(:all)
  end
end
